<?php

namespace Pekkis\Queue\Tests;

use Pekkis\Queue\Adapter\Adapter;
use Pekkis\Queue\Data\SerializedData;
use Pekkis\Queue\Message;
use Pekkis\Queue\Queue;
use Pekkis\Queue\RuntimeException;
use PHPUnit\Framework\MockObject\MockObject;

class QueueTest extends TestCase
{
    private MockObject&Adapter $adapter;
    private Queue $queue;

    public function setUp(): void
    {
        $this->adapter = $this->createMock(Adapter::class);
        $this->queue = new Queue($this->adapter);
    }

    /**
     * @test
     */
    public function enqueueDelegates(): Message
    {
        $this->adapter
            ->expects($this->once())
            ->method('enqueue')
            ->with($this->isType('string'))
            ->willReturnCallback(
                function ($str) {
                    return $str;
                }
            );
        return $this->queue->enqueue('test-message', array('aybabtu' => 'lussentus'));
    }

    /**
     * @test
     */
    public function dequeueDelegates(): void
    {
        $serialized = new SerializedData('Pekkis\Queue\Data\BasicDataSerializer', serialize('lussentus'));
        $arr = array(
            'uuid' => 'uuid',
            'topic' => 'lus.tus',
            'data' => $serialized->toJson(),
        );
        $input = json_encode($arr);

        $this->adapter->expects($this->once())
            ->method('dequeue')
            ->willReturn(array($input, 'aybabtu', []));

        $dequeued = $this->queue->dequeue();
        $this->assertInstanceof(Message::class, $dequeued);

        $this->assertEquals('aybabtu', $dequeued->getIdentifier());
        $this->assertEquals('lus.tus', $dequeued->getTopic());
        $this->assertEquals('lussentus', $dequeued->getData());
    }

    /**
     * @test
     */
    public function dequeueReturnsFalseWhenQueueEmpty(): void
    {
        $this->adapter
            ->method('dequeue')
            ->willReturn(false);

        $this->assertFalse($this->queue->dequeue());
    }

    /**
     * @test
     */
    public function ackDelegates(): void
    {
        $message = Message::create('test-message', array('aybabtu' => 'lussentus'));
        $this->adapter->expects($this->once())->method('ack');

        $this->queue->ack($message);
    }

    /**
     * @test
     */
    public function purgeDelegates(): void
    {
        $this->adapter->expects($this->once())->method('purge')->willReturn(true);
        $this->assertTrue($this->queue->purge());
    }

    /**
     * @test
     */
    public function unknownDataThrowsExceptionWhenSerializing(): void
    {
        $this->expectException(RuntimeException::class);
        $this->queue->enqueue('lus.tus', new RandomBusinessObject());
    }

    /**
     * @test
     */
    public function unknownDataThrowsExceptionWhenUnserializing(): void
    {
        $this->expectException(RuntimeException::class);

        $serialized = new SerializedData('SomeRandomSerializer', 'xooxoo');

        $arr = array(
            'uuid' => 'uuid',
            'topic' => 'lus.tus',
            'data' => $serialized->toJson(),
        );
        $json = json_encode($arr);

        $this->adapter->expects($this->once())
            ->method('dequeue')
            ->willReturn(array($json, 'aybabtu', []));

        $this->queue->dequeue();
    }

    /**
     * @test
     */
    public function addsOutputFilter(): void
    {
        $ret = $this->queue->addOutputFilter(
            function (): void {
            }
        );
        $this->assertSame($this->queue, $ret);
    }

    /**
     * @test
     */
    public function addsInputFilter(): void
    {
        $ret = $this->queue->addInputFilter(
            function (): void {
            }
        );
        $this->assertSame($this->queue, $ret);
    }

    /**
     * @test
     */
    public function returnsAdapter(): void
    {
        $this->assertSame($this->adapter, $this->queue->getAdapter());
    }

    /**
     * @test
     * @group lussi
     */
    public function enqueueThrowsIfAdapterThrows(): void
    {
        $this->adapter
            ->method('enqueue')
            ->willThrowException(
                new \RuntimeException('test exception')
            );

        $this->expectException(RuntimeException::class);
        $this->queue->enqueue('test-topic', 'test-data');
    }
}
