<?php

namespace Pekkis\Queue\Tests;


class TestCase extends \PHPUnit\Framework\TestCase
{
    public function assertUuid(string $what): void
    {
        $this->assertMatchesRegularExpression(
            '/([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})/',
            $what,
            "'{$what}' is not an UUID"
        );
    }
}
