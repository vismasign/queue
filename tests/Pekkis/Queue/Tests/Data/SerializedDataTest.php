<?php

namespace Pekkis\Queue\Data;

use Pekkis\Queue\RuntimeException;
use PHPUnit\Framework\TestCase;

class SerializedDataTest extends TestCase
{
    /**
     * @test
     *
     */
    public function failsWhenDataCannotBeEncoded(): void
    {
        $this->expectException(RuntimeException::class);
        $serializedData = new SerializedData('tussenhofer', mb_convert_encoding('söösöö', 'ISO-8859-1', 'UTF-8'));
        $serializedData->toJson();
    }
}
