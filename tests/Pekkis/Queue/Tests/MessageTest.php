<?php

namespace Pekkis\Queue\Tests;

use Pekkis\Queue\Message;

class MessageTest extends TestCase
{

    /**
     * @test
     */
    public function initializesProperly(): void
    {
        $topic = 'test';
        $data = array('message' => 'All your base are belong to us');

        $message = Message::create($topic, $data);

        $this->assertEquals($data, $message->getData());
        $this->assertEquals($topic, $message->getTopic());
        $this->assertUuid($message->getUuid());

    }

    /**
     * @test
     */
    public function isRestorableFromArray(): void
    {
        $arr = array(
            'uuid' => 'lussutus-uuid',
            'topic' => 'lussutusviesti',
            'data' => array('lussutappa' => 'tussia'),
        );

        $message = Message::fromArray($arr);

        $this->assertEquals($arr['data'], $message->getData());
        $this->assertEquals($arr['uuid'], $message->getUuid());
        $this->assertEquals($arr['topic'], $message->getTopic());
    }

    /**
     * @test
     */
    public function internalDataWorks(): void
    {
        $message = Message::create('luss', array('mussutus' => 'kovaa mussutusta'));

        $this->assertNull($message->getIdentifier());
        $this->assertSame($message, $message->setIdentifier('loso'));
        $this->assertEquals('loso', $message->getIdentifier());
    }

    /**
     * @test
     */
    public function setDataSetsData(): void
    {
        $message = Message::create('luss', array('mussutus' => 'kovaa mussutusta'));

        $message->setData('lussuttakeepa imaisua');

        $this->assertEquals('lussuttakeepa imaisua', $message->getData());
    }
}
