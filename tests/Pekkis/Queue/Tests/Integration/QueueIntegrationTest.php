<?php

namespace Pekkis\Queue\Tests\Integration;

use Pekkis\Queue\Adapter\PhpAMQPAdapter;
use Pekkis\Queue\Message;
use Pekkis\Queue\Queue;
use Pekkis\Queue\Tests\TestCase;

class QueueIntegrationTest extends TestCase
{
    private Queue $queue;

    public function setUp(): void
    {
        $this->queue = new Queue(
            new PhpAMQPAdapter(
                RABBITMQ_HOST,
                RABBITMQ_PORT,
                RABBITMQ_USERNAME,
                RABBITMQ_PASSWORD,
                RABBITMQ_VHOST,
                'test_exchange',
                'test_queue'
            )
        );
        $this->queue->purge();
    }

    public function provideMessages(): array
    {
        $obj = new \stdClass();
        $obj->lusser = 'nönnönnöö';
        $obj->nuller = null;

        return array(
            array('lussen.meister', array()),
            array('lussen.meister', null),
            array('lussen.meister', 'string-typed message test data'),
            array('lussen.meister', $obj),
        );
    }

    /**
     * @dataProvider provideMessages
     * @test
     */
    public function messagesGoThroughThePipeUnchanged(string $topic, mixed $data): void
    {
        $this->assertFalse($this->queue->dequeue());
        $message = $this->queue->enqueue($topic, $data);

        $dequeued = $this->queue->dequeue();

        $this->assertInstanceOf(Message::class, $dequeued);

        $this->assertEquals($message->getTopic(), $dequeued->getTopic());
        $this->assertEquals($message->getData(), $dequeued->getData());

        $this->queue->ack($dequeued);

        $this->assertFalse($this->queue->dequeue());
    }
}
