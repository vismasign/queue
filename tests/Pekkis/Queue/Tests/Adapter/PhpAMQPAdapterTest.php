<?php

namespace Pekkis\Queue\Tests\Adapter;

use Pekkis\Queue\Adapter\PhpAMQPAdapter;

class PhpAMQPAdapterTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getAdapter(): PhpAMQPAdapter
    {
        return new PhpAMQPAdapter(
            RABBITMQ_HOST,
            RABBITMQ_PORT,
            RABBITMQ_USERNAME,
            RABBITMQ_PASSWORD,
            RABBITMQ_VHOST,
            'test_exchange',
            'test_queue'
        );
    }
}
