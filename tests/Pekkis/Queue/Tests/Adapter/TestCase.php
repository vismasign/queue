<?php

namespace Pekkis\Queue\Tests\Adapter;

use Pekkis\Queue\Adapter\Adapter;

abstract class TestCase extends \Pekkis\Queue\Tests\TestCase
{
    protected Adapter $adapter;
    protected string $message;

    public function setUp(): void
    {
        $this->adapter = $this->getAdapter();
        $this->message = 'test-message';
    }

    abstract protected function getAdapter(): Adapter;

    protected function getSleepyTime(): int
    {
        return 1;
    }

    /**
     * @test
     */
    public function dequeueShouldDequeueEnqueuedMessage(): void
    {
        $this->adapter->purge();

        $this->adapter->enqueue($this->message, 'tenhusen-suuruuden-ylistys');

        $actual = $this->adapter->dequeue();
        $this->assertNotFalse($actual);

        list ($message, $identifier, $internals) = $actual;
        $this->assertIsString($message);

        $this->assertEquals($this->message, $message);

        $this->adapter->ack($identifier, $internals);
        $this->assertFalse($this->adapter->dequeue());
    }

    /**
     * @test
     */
    public function dequeueShouldReturnFalseIfQueueIsEmpty(): void
    {
        $this->adapter->purge();
        $actual = $this->adapter->dequeue();
        $this->assertFalse($actual);
    }

    /**
     * @test
     */
    public function purgeShouldResultInAnEmptyQueue(): void
    {
        $this->adapter->purge();

        for ($x = 10; $x <= 10; $x++) {
            $this->adapter->enqueue("message {$x}", 'tenhunen');
        }

        $actual = $this->adapter->dequeue();
        $this->assertNotFalse($actual);

        list ($message, $identifier, $internals) = $actual;
        $this->assertIsString($message);

        $this->adapter->ack($identifier, $internals);

        $this->adapter->purge();

        $this->assertFalse($this->adapter->dequeue());
    }

    /**
     * @test
     * @runInSeparateProcess
     */
    public function queueShouldResendMessageOnlyIfMessageIsNotAcked(): string
    {
        $queue = $this->getAdapter();
        $queue->purge();

        $this->assertFalse($queue->dequeue());

        $message = 'messago mucho masculino';
        $queue->enqueue($message, 'tenhunen');

        $actual = $queue->dequeue();
        $this->assertNotFalse($actual);

        list ($dequeued, $identifier, $internals) = $actual;
        $this->assertEquals($message, $dequeued);
        $this->assertFalse($queue->dequeue());

        return $message;
    }

    /**
     * @test
     * @depends queueShouldResendMessageOnlyIfMessageIsNotAcked
     * @runInSeparateProcess
     *
     */
    public function queueShouldResendMessageOnlyIfMessageIsNotAcked2(string $message): void
    {
        if ($sleepyTime = $this->getSleepyTime()) {
            sleep($sleepyTime);
        }

        $queue = $this->getAdapter();

        $actual = $queue->dequeue();
        $this->assertNotFalse($actual);

        list ($dequeued, $identifier, $internals) = $actual;
        $this->assertEquals($message, $dequeued);

        $queue->ack($identifier, $internals);
    }

    /**
     * @test
     * @depends queueShouldResendMessageOnlyIfMessageIsNotAcked2
     * @runInSeparateProcess
     *
     */
    public function queueShouldResendMessageOnlyIfMessageIsNotAcked3(): void
    {
        if ($sleepyTime = $this->getSleepyTime()) {
            sleep($sleepyTime);
        }

        $queue = $this->getAdapter();
        $this->assertFalse($queue->dequeue());
    }
}
