<?php

if (!@include __DIR__ . '/../vendor/autoload.php') {
    die("You must set up the project dependencies, run the following commands:
wget http://getcomposer.org/composer.phar
php composer.phar install --dev
");
}

gc_enable();

!defined('ROOT_TESTS') && define('ROOT_TESTS', realpath(__DIR__));

// Autoload test classes
spl_autoload_register(function ($class): void {
    $filename = str_replace("\\", DIRECTORY_SEPARATOR, $class) . '.php';
    if (file_exists(ROOT_TESTS . DIRECTORY_SEPARATOR . $filename)) {
        include_once $filename;
    }
});

!defined('RABBITMQ_HOST') && define('RABBITMQ_HOST', "localhost");
!defined('RABBITMQ_PORT') && define('RABBITMQ_PORT', "");
!defined('RABBITMQ_VHOST') && define('RABBITMQ_VHOST', "");
!defined('RABBITMQ_USERNAME') && define('RABBITMQ_USERNAME', "");
!defined('RABBITMQ_PASSWORD') && define('RABBITMQ_PASSWORD', "");
