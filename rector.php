<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/library',
        __DIR__ . '/tests',
    ])
    ->withSets([
        \Rector\PHPUnit\Set\PHPUnitSetList::PHPUNIT_90,
        \Rector\Set\ValueObject\SetList::PHP_82,
        \Rector\Set\ValueObject\SetList::TYPE_DECLARATION,
        \Rector\Set\ValueObject\SetList::DEAD_CODE,
        \Rector\Set\ValueObject\SetList::EARLY_RETURN,
        \Rector\Set\ValueObject\SetList::INSTANCEOF,
    ]);