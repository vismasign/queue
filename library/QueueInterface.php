<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue;

use Pekkis\Queue\Adapter\Adapter;

interface QueueInterface
{
    /**
     * @throws RuntimeException
     */
    public function enqueue(string $topic, mixed $data = null): Message;

    /**
     * Dequeues message
     */
    public function dequeue(): false|Message;

    /**
     * Purges the queue
     */
    public function purge(): mixed;

    /**
     * Acknowledges message
     */
    public function ack(Message $message): void;

    public function getAdapter(): Adapter;
}
