<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Adapter;

interface Adapter
{
    /**
     * Enqueues a message
     */
    public function enqueue(string $message, string $topic): void;

    /**
     * Dequeues a message
     */
    public function dequeue(): false|array;

    /**
     * Purges the queue
     */
    public function purge(): mixed;

    /**
     * Acknowledges a message
     */
    public function ack(mixed $identifier, array $internals): void;
}
