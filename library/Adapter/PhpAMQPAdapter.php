<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Adapter;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PhpAMQPAdapter implements Adapter
{
    private ?AMQPChannel $channel = null;

    private $exchange;

    private $queue;

    private $exchangeType;

    private $routingKey;

    private $deliveryMode;

    private array $connectionOptions;

    public function __construct(
        $host,
        $port,
        $user,
        $pass,
        $vhost,
        $exchange,
        $queue,
        $exchangeType = 'direct',
        $routingKey = '',
        $deliveryMode = AMQPMessage::DELIVERY_MODE_NON_PERSISTENT
    ) {
        $this->exchange = $exchange;
        $this->exchangeType = $exchangeType;
        $this->queue = $queue;
        $this->routingKey = $routingKey;
        $this->deliveryMode = $deliveryMode;

        $this->connectionOptions = array(
            'host' => $host,
            'port' => $port,
            'user' => $user,
            'pass' => $pass,
            'vhost' => $vhost,
        );
    }

    /**
     * Connects to AMQP and gets channel
     */
    private function getChannel(): AMQPChannel
    {
        if (!$this->channel) {
            $conn = new AMQPStreamConnection(
                $this->connectionOptions['host'],
                $this->connectionOptions['port'],
                $this->connectionOptions['user'],
                $this->connectionOptions['pass'],
                $this->connectionOptions['vhost']
            );
            $ch = $conn->channel();

            $ch->exchange_declare($this->exchange, $this->exchangeType, false, true, false);

            $ch->queue_declare($this->queue, false, true, false, false);

            $ch->queue_bind($this->queue, $this->exchange, $this->routingKey);
            $this->channel = $ch;
        }

        return $this->channel;
    }

    public function enqueue(string $message, string $topic): void
    {
        if (!$this->routingKey) {
            $routingKey = '';
        } else {
            $routingKey = $topic;
        }

        $message = new AMQPMessage(
            $message,
            array('content_type' => 'text/plain', 'delivery_mode' => $this->deliveryMode)
        );
        $this->getChannel()->basic_publish($message, $this->exchange, $routingKey, false, false);
    }

    public function dequeue(): false|array
    {
        $msg = $this->getChannel()->basic_get($this->queue);

        if (!$msg) {
            return false;
        }

        return array(
            $msg->body,
            $msg->delivery_info['delivery_tag'],
            [],
        );
    }

    public function purge(): mixed
    {
        return $this->getChannel()->queue_purge($this->queue);
    }

    public function ack(mixed $identifier, array $internals): void
    {
        $this->getChannel()->basic_ack($identifier);
    }
}
