<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue;

use Closure;
use Pekkis\Queue\Adapter\Adapter;
use Pekkis\Queue\Data\BasicDataSerializer;
use Pekkis\Queue\Data\DataSerializer;
use Pekkis\Queue\Data\DataSerializers;
use Pekkis\Queue\Data\SerializedData;
use Pekkis\Queue\Filter\InputFilters;
use Pekkis\Queue\Filter\OutputFilters;

class Queue implements QueueInterface
{
    private Adapter $adapter;
    private DataSerializers $dataSerializers;
    private OutputFilters $outputFilters;
    private InputFilters $inputFilters;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;

        $this->dataSerializers = new DataSerializers();

        $this->outputFilters = new OutputFilters();
        $this->inputFilters = new InputFilters();

        $this->addDataSerializer(new BasicDataSerializer());
    }

    /**
     * @throws RuntimeException
     */
    public function enqueue(string $topic, mixed $data = null): Message
    {
        $serializer = $this->dataSerializers->getSerializerFor($data);
        if (!$serializer) {
            throw new RuntimeException("Serializer not found");
        }
        $serializedData = new SerializedData($serializer->getIdentifier(), $serializer->serialize($data));

        $message = Message::create($topic, $data);
        $arr = array(
            'uuid' => $message->getUuid(),
            'topic' => $message->getTopic(),
            'data' => $serializedData->toJson(),
        );

        $json = json_encode($arr);
        $json = $this->outputFilters->filter($json);

        try {
            $this->adapter->enqueue($json, $topic);
            return $message;
        } catch (\Exception $e) {
            throw new RuntimeException('Queuing a message failed', 0, $e);
        }
    }

    /**
     * Dequeues message
     */
    public function dequeue(): false|Message
    {
        $raw = $this->adapter->dequeue();
        if (!$raw) {
            return false;
        }

        list ($json, $identifier, $internals) = $raw;
        $json = $this->inputFilters->filter($json);
        $json = json_decode($json, true);
        $serialized = SerializedData::fromJson($json['data']);
        $serializer = $this->dataSerializers->getUnserializerFor($serialized);

        if (!$serializer) {
            throw (new RuntimeException('Unserializer not found'))->setContext($raw);
        }

        $json['data'] = $serializer->unserialize($serialized->getData());
        $message = Message::fromArray($json);
        $message->setIdentifier($identifier);

        foreach ($internals as $key => $value) {
            $message->setInternal($key, $value);
        }

        return $message;
    }

    /**
     * Purges the queue
     */
    public function purge(): mixed
    {
        return $this->adapter->purge();
    }

    /**
     * Acknowledges message
     */
    public function ack(Message $message): void
    {
        $this->adapter->ack($message->getIdentifier(), $message->getInternals());
    }

    public function addDataSerializer(DataSerializer $dataSerializer): static
    {
        $this->dataSerializers->add($dataSerializer);
        return $this;
    }

    public function addOutputFilter(Closure $callable): static
    {
        $this->outputFilters->add($callable);
        return $this;
    }

    public function addInputFilter(Closure $callable): static
    {
        $this->inputFilters->add($callable);
        return $this;
    }

    public function getAdapter(): Adapter
    {
        return $this->adapter;
    }
}
