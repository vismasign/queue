<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Filter;

use ArrayIterator;
use Closure;
use IteratorAggregate;

class OutputFilters implements IteratorAggregate
{
    /**
     * @var Closure[] $filters
     */
    protected array $filters = [];

    public function getIterator(): \Traversable
    {
        return new ArrayIterator($this->filters);
    }

    public function add(Closure $callable): static
    {
        $this->filters[] = $callable;
        return $this;
    }

    public function filter(string $str): string
    {
        foreach ($this as $filter) {
            $str = $filter($str);
        }

        return $str;
    }
}
