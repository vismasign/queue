<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue;

use Ramsey\Uuid\Uuid;

class Message
{
    private string $topic;
    private string $uuid;
    private mixed $data;
    private array $internal = [];

    private function __construct(string $uuid, string $topic, mixed $data)
    {
        $this->uuid = $uuid;
        $this->topic = $topic;
        $this->data = $data;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): void
    {
        $this->data = $data;
    }

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setInternal(string $key, mixed $value): self
    {
        $this->internal[$key] = $value;
        return $this;
    }

    public function getInternal(string $key, mixed $default = null): mixed
    {
        if (!isset($this->internal[$key])) {
            return $default;
        }
        return $this->internal[$key];
    }

    public function getInternals(): array
    {
        return $this->internal;
    }

    public function getIdentifier(): mixed
    {
        return $this->getInternal('identifier');
    }

    public function setIdentifier(string $identifier): static
    {
        $this->setInternal('identifier', $identifier);
        return $this;
    }

    public static function create(string $topic, mixed $data = null): self
    {
        return new self(Uuid::uuid4()->toString(), $topic, $data);
    }

    public static function fromArray(array $arr): self
    {
        return new self($arr['uuid'], $arr['topic'], $arr['data']);
    }
}
