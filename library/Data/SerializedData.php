<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Data;

use Pekkis\Queue\RuntimeException;

class SerializedData
{
    private string $serializerIdentifier;
    private string $data;

    public final function __construct(string $serializerIdentifier, string $data)
    {
        $this->serializerIdentifier = $serializerIdentifier;
        $this->data = $data;
    }

    public function getSerializerIdentifier(): string
    {
        return $this->serializerIdentifier;
    }

    public function getData(): string
    {
        return $this->data;
    }

    public function toJson(): string
    {
        $encoded = json_encode(
            array(
                'serializerIdentifier' => $this->serializerIdentifier,
                'data' => $this->data,
            )
        );

        if (false === $encoded) {
            throw new RuntimeException(
                sprintf("Failed to JSON encode serialized data with '%s'", $this->data)
            );
        }

        return $encoded;
    }

    public static function fromJson(string $json): static
    {
        $decoded = json_decode($json, true);
        return new static(
            $decoded['serializerIdentifier'],
            $decoded['data']
        );
    }
}
