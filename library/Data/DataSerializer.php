<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Data;

interface DataSerializer
{
    public function willSerialize(mixed $unserialized): bool;

    public function serialize(mixed $unserialized): string;

    public function unserialize(string $serialized): mixed;

    public function getIdentifier(): string;
}
