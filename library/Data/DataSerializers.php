<?php

/**
 * This file is part of the pekkis-queue package.
 *
 * For copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pekkis\Queue\Data;

class DataSerializers
{
    /**
     * @var DataSerializer[] $serializers
     */
    private array $serializers = [];

    public function add(DataSerializer $serializer): void
    {
        $this->serializers[] = $serializer;
    }

    /**
     * @return DataSerializer[]
     */
    public function getSerializers(): array
    {
        return $this->serializers;
    }

    protected function getSerializerByIdentifier(string $identifier): false|DataSerializer
    {
        foreach ($this->getSerializers() as $serializer) {
            if ($serializer->getIdentifier() === $identifier) {
                return $serializer;
            }
        }
        return false;
    }

    public function getSerializerFor(mixed $unserialized): false|DataSerializer
    {
        foreach (array_reverse($this->getSerializers()) as $serializer) {
            if ($serializer->willSerialize($unserialized)) {
                return $serializer;
            }
        }
        return false;
    }

    public function getUnserializerFor(SerializedData $data): false|DataSerializer
    {
        return $this->getSerializerByIdentifier($data->getSerializerIdentifier());
    }
}
